FROM alpine



RUN apk add --no-cache \
        ansible \
        curl \
        git \
        openssh \
        libstdc++ \
        bash